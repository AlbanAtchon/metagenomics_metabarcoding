library(futile.logger)
suppressMessages(library("optparse"))

option_list = list(
	make_option(c("-r", "--rdata"), type="character", default=NULL,
              help="Rdata file path.", metavar="path"),
	make_option(c("-o", "--out"), type="character", default="./tree",
              help="output .Rdata file name [default= %default]", metavar="path")
);

opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

if (is.null(opt$rdata)){
	print_help(opt_parser)
	stop("You must provide the Rdata file path.", call.=FALSE)
} else{
	flog.info("Loading data...")
	load(opt$rdata)
	flog.info('Done.')
}

flog.info("Loading libraries...")
suppressMessages(library(dada2))
suppressMessages(library(phyloseq))
suppressMessages(library(DECIPHER))
suppressMessages(library(ShortRead))
suppressMessages(library(Biostrings))
suppressMessages(library(digest))
suppressMessages(library(phangorn))
flog.info('Done.')



flog.info('Generating tree...')
sequences <- getSequences(seqtab.nochim)
names(sequences) <- sapply(sequences,digest,algo='md5')
flog.info('Aligning sequences...')
alignment <- AlignSeqs(DNAStringSet(sequences),anchor=NA,processors=NULL)
flog.info('Creating distance matrices...')
phang.align <- phyDat(as(alignment, "matrix"), type="DNA")
dm <- dist.ml(phang.align)
flog.info('Neigbour joining...')
treeNJ <- NJ(dm) # Note, tip order != sequence order
fit = pml(treeNJ, data=phang.align)

flog.info('GTR...')
fitGTR <- update(fit, k=4, inv=0.2)
# fitGTR <- optim.pml(fitGTR, model="GTR", optInv=TRUE, optGamma=TRUE, rearrangement = "stochastic", control = pml.control(epsilon = 1e-08, maxit = 10,trace = 0))
detach("package:phangorn", unload=TRUE)
flog.info('Done.')

tree <- fitGTR$tree

if(!dir.exists(opt$out)){
	flog.info('Creating output directory...')
	dir.create(opt$out)
	flog.info('Done.')
}
flog.info('Saving R objects.')
save(tree, file=paste(opt$out,'/tree_robjects.Rdata',sep=''))
flog.info('Finish.')
