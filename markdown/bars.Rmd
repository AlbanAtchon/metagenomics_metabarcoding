---
title: Bar plot
fig_width: 24
params:
  rmd_data: p
  col1: col1
---

```{r message=FALSE, warning=FALSE, include=FALSE, results='hide'}
rmd_data <- params$rmd_data

```


```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 10, fig.height = 10}
if('rare' %in% names(rmd_data)){
  cat('# Rarefaction plot')
  cat("\n")
  rmd_data$rare
}
```


```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('bars' %in% names(rmd_data)){
  cat('# Plot raw value composition')
  cat("\n")
}
```

```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('Phylum' %in% names(rmd_data$bars)){
  cat('## Phylum')
  cat("\n")
  rmd_data$bars$Phylum
}
```
```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('Class' %in% names(rmd_data$bars)){
  cat('## Class')
  cat("\n")
  rmd_data$bars$Class
  cat("\n")
}
```
```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('Order' %in% names(rmd_data$bars)){
  cat('## Order')
  cat("\n")
  rmd_data$bars$Order
}
```
```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('Family' %in% names(rmd_data$bars)){
  cat("## Family")
  cat("\n")
  rmd_data$bars$Family
}
```
```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('Genus' %in% names(rmd_data$bars)){
  cat("## Genus")
  cat("\n")
  rmd_data$bars$Genus
}
```


```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('compo' %in% names(rmd_data)){
  cat('# Composition plot')
  cat("\n")
}
```

```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('Phylum' %in% names(rmd_data$compo)){
  cat('## Phylum')
  cat("\n")
  rmd_data$compo$Phylum
}
```

```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('Class' %in% names(rmd_data$compo)){
  cat('## Class')
  cat("\n")
  rmd_data$compo$Class
}
```

```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('Order' %in% names(rmd_data$compo)){
  cat('## Order')
  cat("\n")
  rmd_data$compo$Order
}
```

```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('Family' %in% names(rmd_data$compo)){
  cat("## Family")
  cat("\n")
  rmd_data$compo$Family
}
```


```{r hold=TRUE, echo=FALSE, comment = FALSE, message= FALSE, warning = FALSE, results='asis',fig.keep='all', fig.align='left', fig.width = 20, fig.height = 10}
if('Genus' %in% names(rmd_data$compo)){
  cat("## Genus")
  cat("\n")
  rmd_data$compo$Genus
}
```
