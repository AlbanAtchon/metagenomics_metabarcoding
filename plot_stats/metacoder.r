library(futile.logger)
suppressMessages(library("optparse"))

option_list = list(
  make_option(c("-r", "--rdata"), type="character", default=NULL,
              help="Rdata file path.", metavar="path"),
	make_option(c("-o", "--out"), type="character", default="./metacoder/",
              help="output .Rdata file name. [default= %default]", metavar="path"),
	make_option(c("-a", "--column1"), type="character", default="type",
              help="Column name. [default= %default]", metavar="STR"),
	make_option(c("-b", "--column2"), type="character", default="",
              help="Column name. [default= %default]", metavar="STR"),
	make_option(c("-i", "--rank"), type="character", default="Species",
              help="Taxonomic rank name. Use 'ASV' for no glom. [default= %default]", metavar="STR"),
	make_option(c("-s", "--signif"), type="logical", default="TRUE",
              help="Only significant. [default= %default]", metavar="STR"),
	make_option(c("-p", "--plottrees"), type="logical", default="FALSE",
              help="Plot trees (long treatments). [default= %default]", metavar="STR"),
	make_option(c("-m", "--min"), type="integer", default="1000",
              help="Minimum number of reads for a taxa to be represented. [default= %default]", metavar="STR"),
	make_option(c("-c", "--comp"), type="character", default="all",
	            help="Comma separated list of comparison to test. Comparisons are informed with a tilde (A~C,A~B,B~C). If empty, test all combination [default= %default]", metavar="STR")
);

opt_parser = OptionParser(option_list=option_list)
opt = parse_args(opt_parser)

if (is.null(opt$rdata)){
  print_help(opt_parser)
  stop("You must provide the Rdata file path.", call.=FALSE)
}

flog.info('Loading librairies ...')
suppressMessages(library('phyloseq'))
suppressMessages(library('ggplot2'))
suppressMessages(library('metacoder'))
library(gridExtra)
library(grid)
flog.info('Done.')
load(opt$rdata)

if(!dir.exists(opt$out)){
	dir.create(opt$out, recursive = TRUE)
}


set.seed(1)


mean_ratio <- function(abund_1, abund_2) {
  log_ratio <- log2(mean(abund_1) / mean(abund_2))
  if (is.nan(log_ratio)) {
    log_ratio <- 0
  }
  list(log2_mean_ratio = log_ratio,
       median_diff = median(abund_1) - median(abund_2),
       mean_diff = mean(abund_1) - mean(abund_2),
       wilcox_p_value = wilcox.test(abund_1, abund_2)$p.value)
}


launch_metacoder <- function(psobj, col, rank, var, title, plot1){

	vector <- unique(data.frame(sample_data(psobj)[,col])[,1])


	flog.info('Transform sample counts...')

	normf = function(x, tot=max(sample_sums(psobj))){ tot*x/sum(x) }
	psobj <- transform_sample_counts(psobj, normf)
	if(rank != '' & rank != 'ASV'){
		psobj <- tax_glom(psobj, taxrank=rank)
	}
	flog.info('Zeroing low counts...')
	obj <- parse_phyloseq(psobj, class_regex = "(.*)", class_key = "taxon_name")
	obj$data$otu_table <- zero_low_counts(obj, "otu_table", min_count = opt$min, use_total = TRUE)
	no_reads <- rowSums(obj$data$otu_table[, obj$data$sample_data$sample_id]) == 0
	obj <- filter_obs(obj, "otu_table", ! no_reads, drop_taxa = TRUE)
  if(nrow(obj$data$otu_table)==0){return(NULL)}

	flog.info('Calculating taxon abundance...')
	obj$data$tax_abund <- calc_taxon_abund(obj, "otu_table",  cols = obj$data$sample_data$sample_id)
	obj$data$tax_abund$total <- rowSums(obj$data$tax_abund[, -1]) # -1 = taxon_id column
	obj$data$n_samples <- calc_n_samples(obj,data="tax_abund")

	flog.info('Comparing groups...')
	fun <- paste('obj$data$diff_table <- compare_groups(obj, data = "tax_abund", cols = obj$data$sample_data$sample_id, groups = obj$data$sample_data$', col, ',func = mean_ratio)', sep='')
	eval(parse(text=fun))

	# obj$data$diff_table$log2_median_ratio[is.infinite(obj$data$diff_table$log2_median_ratio)] <- 0
	table <- merge(obj$data$diff_table, obj$data$tax_data,by='taxon_id')
	# write.table(merge(obj$data$diff_table, obj$data$tax_data,by='taxon_id'),file = paste(opt$out,'/metacoder_',var,'_',col,'_',rank,'.csv',sep=''),sep="\t", col.names=NA)

# Write signif table???
  # return(obj)
  # # save.image("debug.rdata")
  # # diffT <- obj$data$diff_table
  # # taxT <- obj$data$tax_data
  # #
  # # tt=na.omit(diffT[diffT$wilcox_p_value <=0.05,])
  # # na.omit(taxT[diffT$wilcox_p_value <=0.05,])
  # write.table(merge(obj$data$diff_table[obj$data$diff_table$wilcox_p_value<0.05,], obj$data$tax_data[obj$data$diff_table$wilcox_p_value<0.05,],by='taxon_id'),file = paste(opt$out,'/metacoder_signif_',var,'_',col,'_',rank,'.csv',sep=''),sep="\t", col.names=NA)
if(plot1 == TRUE){
  flog.info('Plotting all...')
  col_range <-  c("cyan", "gray", "tan")


  if(length(vector) == 2){
    fun <- paste0('p1 <- heat_tree(obj,
      node_label = taxon_names,
      node_size = total,
      node_color = log2_mean_ratio,
      node_color_interval = c(-3,3),
      edge_color_interval = c(-3, 3),
      node_color_range = diverging_palette(),
      node_color_trans = "linear",
      node_size_axis_label = "Size: Number reads",
      node_color_axis_label = "Color: Log2mean brown ',vector[1],', green: ',vector[2],'",
      layout = "davidson-harel",
      initial_layout = "reingold-tilford",
      repel_labels = TRUE,
      repel_force = 3,
      overlap_avoidance = 3,
      node_label_size_range=c(0.01,0.04),
      make_edge_legend=FALSE) + ggtitle("',title,'") + theme(plot.title = element_text(size = 32, face = "bold"))', sep ='')
    }

    if(length(vector) > 2){
      fun <- paste0('p1 <- heat_tree_matrix(obj,
        data="diff_table",
        node_label = taxon_names,
        node_size = total,
        node_color = log2_mean_ratio,
        node_color_interval = c(-3,3),
        edge_color_interval = c(-3, 3),
        node_color_range = diverging_palette(),
        node_color_trans = "linear",
        node_size_axis_label = "Size: Number of reads",
        node_color_axis_label = "Color: Log 2 ratio of mean proportions",
        layout = "davidson-harel",
        initial_layout = "reingold-tilford",
        repel_labels = TRUE,
        repel_force = 3,
        overlap_avoidance = 3,
        make_edge_legend=FALSE,
        node_label_size_range=c(0.01,0.04),
        row_label_size=16,
        col_label_size=16,
        tree_label_size = 12) + ggtitle("',title,'") + theme(plot.title = element_text(size = 32, face = "bold"))', sep ='')
      }
      eval(parse(text=fun))
      flog.info('Done.')

      if( opt$signif==TRUE){
        obj$data$diff_table$log2_mean_ratio[obj$data$diff_table$wilcox_p_value > 0.05 | is.na(obj$data$diff_table$wilcox_p_value)] <- 0

        list_taxon <- taxon_names(obj)
        signif_taxon <- obj$data$diff_table$taxon_id[obj$data$diff_table$wilcox_p_value < 0.05 & !is.na(obj$data$diff_table$wilcox_p_value)]
        list_taxon[setdiff(names(list_taxon),signif_taxon)] <- NA


        flog.info('Plotting only significant...')
        if(length(vector) == 2){
          fun <- paste0('p2 <- heat_tree(obj,
            node_label = list_taxon,
            node_size = total,
            node_color = log2_mean_ratio,
            node_color_interval = c(-3,3),
            edge_color_interval = c(-3, 3),
            node_color_range = diverging_palette(),
            node_color_trans = "linear",
            node_size_axis_label = "Size: Number of reads",
            node_color_axis_label = "Color: Log2mean brown ',vector[1],', green: ',vector[2],'",
            layout = "davidson-harel",
            initial_layout = "reingold-tilford",
            make_edge_legend=FALSE,
            overlap_avoidance = 3,
            repel_labels = TRUE,
            repel_force = 3,
            node_label_size_range=c(0.02,0.04)) + ggtitle("',title,' (Only significants)") + theme(plot.title = element_text(size = 32, face = "bold"))', sep ='')
          }
          if(length(vector) > 2){
            fun <- paste0('p2 <- heat_tree_matrix(obj,
              data="diff_table",
              node_label = list_taxon,
              node_size = total,
              node_color = log2_mean_ratio,
              node_color_interval = c(-3,3),
              edge_color_interval = c(-3, 3),
              node_color_range = diverging_palette(),
              node_color_trans = "linear",
              node_size_axis_label = "Size: Number of reads",
              node_color_axis_label = "Color: Log 2 ratio of mean proportions",
              layout = "davidson-harel",
              initial_layout = "reingold-tilford",
              repel_labels = TRUE,
              repel_force = 3,
              overlap_avoidance = 3,
              make_edge_legend=TRUE,
              node_label_size_range=c(0.03,0.04),
              row_label_size=16,
              col_label_size=16,
              tree_label_size = 12) + ggtitle("',title,' (Only significants)") + theme(plot.title = element_text(size = 32, face = "bold"))', sep ='')
            }
            eval(parse(text=fun))
            flog.info('Done.')
            # p <- list(p1,p2)
            p <- list(p1,p2,table)
            return (p)
          }else{

            return(p1)
          }

} else {
  p1=NULL;p2=NULL
  p <- list(p1,p2,table)
  return(p)
}


}


if(opt$column2 != ""){
  vector <- levels(data.frame(sample_data(data)[,opt$column1])[,1])
  data_tmp <- data
  for (var in vector){
    flog.info('Split table %s...', var)
  	fun <- paste('psobj <- subset_samples(data, ',opt$column1,'=="',var,'")',sep='')
    eval(parse(text=fun))
    psobj <- prune_taxa(taxa_sums(psobj) >= 1, psobj)
    flog.info('Done.')
    title = paste('Comparison ',var,' ',opt$column2,opt$rank,sep='')
    gg <- launch_metacoder(psobj, opt$column2, opt$rank, var, title, plot1=opt$plottrees)
    plots = marrangeGrob(grobs=gg[1:2],nrow=1,ncol=2)
    ggsave(paste0(opt$out,'/metacoder_',opt$column2,'_',var,'_',opt$rank,'.png'),plot=plots, width=30, height=16, dpi = 320)
  }
}else {
	if(opt$comp == "matrix"){
    flog.info("Comparison in matrix...")
    title = paste('Matrix column ',opt$column1,' at rank ',opt$rank,sep='')
		gg <- launch_metacoder(data, opt$column1, opt$rank, '', title, plot1=opt$plottrees)

    plots = marrangeGrob(grobs=gg[1:2],nrow=1,ncol=2)
		ggsave(paste(opt$out,'/metacoder_',opt$column1,'_',opt$rank,'.png',sep=''),plot=plots, width=30, height=16, dpi = 320)
	}
	else{
    if(opt$comp == "all"){
      flog.info("Comparison in all mode...")
      fun <- paste('combinaisons <- combn(na.omit(unique(sample_data(data)$',opt$column1,')),2) ',sep='')
      eval(parse(text=fun))
    }
    else {
      flog.info('Comparison mode...')
      comp_list <- unlist(strsplit(opt$comp,","))
      combinaisons <- matrix(, nrow = 2, ncol = length(comp_list))
      for (i in 1:length(comp_list)){
        tmp <- unlist(strsplit(comp_list[i],"\\~"))
        # cbind(combinaisons,tmp)
        combinaisons[1,i] <- tmp[1]
        combinaisons[2,i] <- tmp[2]
      }
    }
		p_list <- c()
    table <- data.frame()
		'%!in%' <- function(x,y)!('%in%'(x,y))
		for (comp in (1:ncol(combinaisons))){
      flog.info(paste('Comparison ...',combinaisons[1,comp], combinaisons[2,comp]))
			fun <- paste('tmp <- sample_data(data)$',opt$column1,sep='')
			eval(parse(text=fun))
			if((combinaisons[1,comp] %!in% tmp) || combinaisons[2,comp] %!in% tmp){
				flog.warn(paste(combinaisons[1,comp],' not in sample_data. Next;'),sep='')
				next
			}
			fun <- paste('psobj <- subset_samples(data, ',opt$column1,' %in% c("',combinaisons[1,comp],'","',combinaisons[2,comp],'"))',sep='')
			eval(parse(text=fun))
			title = paste(combinaisons[1,comp], ' VS ', combinaisons[2,comp],sep='')
			pp <- launch_metacoder(psobj, opt$column1, opt$rank, '', title, plot1=opt$plottrees)

      table <- rbind(table,pp[[3]])
			p_list <- c(p_list,pp[1:2])

		}

    flog.info('Output...')
    write.table(table,file = paste(opt$out,'/metacoder_signif_',opt$rank,'.csv',sep=''),sep="\t",row.names=FALSE)

    if(opt$plottrees == TRUE){
      plots = marrangeGrob(grobs=p_list,nrow=1,ncol=2)
      ggsave(paste(opt$out,'/metacoder_',opt$column1,'_',opt$rank,'.pdf',sep=''), plots, width = 40, height = 20)
    }
	}
}


flog.info('Finish.')
