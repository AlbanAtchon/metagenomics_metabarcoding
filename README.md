# ANOMALY : AmplicoN wOrkflow for Microbial community AnaLYsis

:bangbang: :information_source: **ANOMALY is now packaged in an rANOMALY Rpackage. This repository is no longer maintained, please visit [https://forgemia.inra.fr/umrf/ranomaly](https://forgemia.inra.fr/umrf/ranomaly)**

ANOMALY is a simple and customizable R workflow, that uses ASVs level for community characterization and integrates all assets of the up-to-date methods such as better sequence tracking, decontamination, merged taxonomic annotation, statistical tests, and cross-validated differential analysis.
